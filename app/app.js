

//Primary angular module
AJ.Module = angular.module('aj', []);

AJ.ModuleRootScope = null;

//Redraw non dc.js data
AJ.Redraw = function() {

	AJ.ModuleRootScope.DailyChangeHeatmap = AJ.DateWeekDimensionSumPercentChangeByDay.all();

	var events_group = AJ.DateDimensionSums.all();

	AJ.ModuleRootScope.EventsSum = 0;
	AJ.ModuleRootScope.ClosedEventsSum = 0;
	AJ.ModuleRootScope.NotClosedEventsSum = 0;
	AJ.ModuleRootScope.InErrorStateEventsSum = 0;

	AJ.ModuleRootScope.MaxEventsPerDay = null;
	AJ.ModuleRootScope.MinEventsPerDay = null;

	for (var i=0; events_group[i]; i++) {

		var events = parseInt(events_group[i].value['# Events']) || 0;
		var closed_events = parseInt(events_group[i].value['# Closed Events']) || 0;
		var not_closed_events = parseInt(events_group[i].value['# Not Closed']) || 0;
		var in_error_state_events = parseInt(events_group[i].value['# In Error State']) || 0;

		AJ.ModuleRootScope.EventsSum += events;
		AJ.ModuleRootScope.ClosedEventsSum += closed_events;
		AJ.ModuleRootScope.NotClosedEventsSum += not_closed_events;
		AJ.ModuleRootScope.InErrorStateEventsSum += in_error_state_events;

		if (AJ.ModuleRootScope.MaxEventsPerDay === null || AJ.ModuleRootScope.MaxEventsPerDay < events) {

			AJ.ModuleRootScope.MaxEventsPerDay = events;

		}

		if (AJ.ModuleRootScope.MinEventsPerDay === null || AJ.ModuleRootScope.MinEventsPerDay > events) {

			AJ.ModuleRootScope.MinEventsPerDay = events;

		}

	}

	AJ.ModuleRootScope.$apply();

};

AJ.Module.controller('dailyChangeHeatmapCtrl', function($rootScope, $scope) {

	AJ.ModuleRootScope = $rootScope;

	$scope.getCellColor = function(object) {

		if (!object.Count) return 'transparent';

		var percent = Math.abs(object.PercentChangeSum) || 0;

		if (percent > 1) percent = 1;

		//'rgb(236,231,242)','rgb(166,189,219)','rgb(43,140,190)'

		return 'rgb('+Math.floor(43+(236-43)*(1-percent))+','+Math.floor(140+(231-140)*(1-percent))+','+Math.floor(190+(242-190)*(1-percent))+')';


	};

});

AJ.Module.controller('eventsCtrl', function($scope) {

});

AJ.Module.controller('dailyEventsCtrl', function($scope) {

	$scope.getCellColor = function(object) {

		if (!object.Count) return 'transparent';

		var percent = (object.EventsSum - AJ.ModuleRootScope.MinEventsPerDay) / (AJ.ModuleRootScope.MaxEventsPerDay - AJ.ModuleRootScope.MinEventsPerDay);

		if (percent > 1) percent = 1;

		//'rgb(255,255,255)','rgb(255,179,0)'

		return 'rgb(255,'+Math.floor(179+(255-179)*(1-percent))+','+Math.floor(0+(255-0)*(1-percent))+')';

	};

});