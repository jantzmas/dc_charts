
//Primary application namespace
var AJ = {};

//Configuration variables
AJ.Config = {

	CSVDataUrl: 'data/data.csv',

	CSVTestDataUrl: 'data/data.csv'

};

//Crossfilter data object
AJ.CFData = null;

//Links to DOM elements
AJ.Elements = {};

//Parse dates function
AJ.ParseDate = d3.time.format("%m/%d/%y");

//Starting application function (run on onload)
AJ.Start = function() {

	//Save links to needed elements
	AJ.ProcessLinks(document.getElementById('aj-dashboard'), AJ.Elements);

	//Load data from server
	AJ.LoadData(AJ.Config.CSVDataUrl, function(data) {

		AJ.TransformData(data);

		//Load data from server
		AJ.LoadData(AJ.Config.CSVTestDataUrl, function(data) {

			//AJ.TransformTestData(data);

			AJ.SetDataToCharts();

		});

	});

	//Create charts before data loading
	AJ.CreateCharts();

};

//Load data for charts from server
AJ.LoadData = function(file, func) {

	d3.csv(file, function (error, data) {

		func(data);

	});

};

//Transform data fields format
AJ.TransformData = function(data) {

	data.forEach(function(d) {

		d["Date"] = AJ.ParseDate.parse(d["DayDate"]);

		d["# Rules"] = parseInt(d["# Rules"]) || 0;

		d["consumption"] = +d["consumption"];

		d["Percent Change"] = parseFloat(d["Percent Change"]) || 0;

	});

	AJ.CFData = crossfilter(data);

	AJ.ConsumptionDimension = AJ.CFData.dimension(function(d) { return d["consumption"]; });

	AJ.ConsumptionDimensionSumByPercentChange = AJ.ConsumptionDimension.group().reduceSum(function(d) { return d["Percent Change"]; });

	AJ.FacilityNameDimension = AJ.CFData.dimension(function(d) { return d["Facility_Name"]; });

	AJ.FacilityNameDimensionSum = AJ.FacilityNameDimension.group().reduce(

		function(g, d) {

			g["ConsumptionSum"] += d["consumption"];

			g["PercentChangeSum"] += d["Percent Change"];

			g["EventsSum"] += parseInt(d['# Events']) || 0;

			return g;

		},
		function(g, d) {

			g["ConsumptionSum"] -= d["consumption"];

			g["PercentChangeSum"] -= d["Percent Change"];

			g["EventsSum"] -= parseInt(d['# Events']) || 0;

			return g;

		},
		function(d) {

			return {
				ConsumptionSum: 0,
				PercentChangeSum: 0,
				EventsSum: 0
			};

		}

	);

	AJ.DateDimension = AJ.CFData.dimension(function(d) {

		return d.Date ? d.Date.getTime() / (24 * 3600 * 1000) : null;

	});

	AJ.DateDimensionSums = AJ.DateDimension.group().reduce(

		function(g, d) {

			g['# Events'] += parseInt(d['# Events']) || 0;

			g['# Closed Events'] += parseInt(d['# Closed Events']) || 0;

			g['# In Error State'] += parseInt(d['# In Error State']) || 0;

			g['# Not Closed'] += parseInt(d['# Not Closed']) || 0;

			return g;

		},

		function(g, d) {

			g['# Events'] -= parseInt(d['# Events']) || 0;

			g['# Closed Events'] -= parseInt(d['# Closed Events']) || 0;

			g['# In Error State'] -= parseInt(d['# In Error State']) || 0;

			g['# Not Closed'] -= parseInt(d['# Not Closed']) || 0;

			return g;

		},

		function() {

			return {
				"# Events": 0,
				"# Closed Events": 0,
				"# In Error State": 0,
				"# Not Closed": 0
			};

		}

	);

	AJ.DateWeekDimension = AJ.CFData.dimension(function(d) {

		if (!d.Date) return null;

		var day = d.Date.getDay(),

		diff = d.Date.getDate() - day + (day == 0 ? -6 : 1);

		var date = new Date(d.Date.getTime());

		date.setDate(diff);

		return date;

	});

	AJ.DateWeekDimensionSumPercentChangeByDay = AJ.DateWeekDimension.group().reduce(

		function(g, d) {

			if (d.Date) {

				g[d.Date.getDay()].Count++;

				g[d.Date.getDay()].PercentChangeSum += d["Percent Change"];

				g[d.Date.getDay()].EventsSum += parseInt(d['# Events']) || 0;

			}

			return g;

		},
		function(g, d) {

			if (d.Date) {

				g[d.Date.getDay()].Count--;

				g[d.Date.getDay()].PercentChangeSum -= d["Percent Change"];

				g[d.Date.getDay()].EventsSum -= parseInt(d['# Events']) || 0;

			}

			return g;

		},
		function() {

			return {
				"0": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"1": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"2": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"3": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"4": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"5": {PercentChangeSum: 0, Count: 0, EventsSum: 0},
				"6": {PercentChangeSum: 0, Count: 0, EventsSum: 0}
			};

		}

	);

	AJ.RulesDimension = AJ.CFData.dimension(function(d) { return d["# Rules"]; });

	AJ.RulesDimensionSum = AJ.RulesDimension.group().reduce(

		function(g, d) {

			g["Count"] += 1;

			g["EventsSum"] += parseInt(d['# Events']) || 0;

			return g;

		},
		function(g, d) {

			g["Count"] -= 1;

			g["EventsSum"] -= parseInt(d['# Events']) || 0;

			return g;

		},
		function(d) {

			return {
				Count: 0,
				EventsSum: 0
			};

		}

	);

};

//Transform test data fields format
AJ.TransformTestData = function(data) {

	data.forEach(function(d) {

		d["Key1"] = +d["Key1"];

		d["consumption"] = +d["consumption"];

		d["Date"] = AJ.ParseDate.parse(d["Date"]);

	});

	AJ.CFTestData = crossfilter(data);

	AJ.TestDateWeekDimension = AJ.CFTestData.dimension(function(d) {

		var day = d.Date.getDay(),

		diff = d.Date.getDate() - day + (day == 0 ? -6 : 1);

		var date = new Date(d.Date.getTime());

		date.setDate(diff);

		return date;

	});

	AJ.TestDateWeekDimensionSumPercentChangeByDay = AJ.TestDateWeekDimension.group().reduce(

		function(g, d) {

			g[d.Date.getDay()] += d["Percent Change"];

			return g;

		},

		function(g, d) {

			g[d.Date.getDay()] -= d["Percent Change"];

			return g;

		},

		function() {

			return {
				"0": 0,
				"1": 0,
				"2": 0,
				"3": 0,
				"4": 0,
				"5": 0,
				"6": 0
			};

		}

	);

};

//Create D3 chart objects
AJ.CreateCharts = function() {

	AJ.CreateKwhVsChangeChart();

	AJ.CreateTopTrendingOverBaseLineChart();

	AJ.CreateTopTrendingUnderBaseLineChart();

	AJ.CreateTopFacilityChart();

	AJ.CreateTopRulesChart();

	AJ.CreateEventsChart();

	AJ.CreateClosedEventsChart();

	AJ.CreateNotClosedEventsChart();

	AJ.CreateInErrorStateEventsChart();

	AJ.CreateQuantityVsVarietyChart();

};

//Set real data to charts and draw it
AJ.SetDataToCharts = function() {

	AJ.SetKwhVsChangeChartData();

	AJ.SetTopTrendingOverBaseLineChartData();

	AJ.SetTopTrendingUnderBaseLineChartData();

	AJ.SetTopFacilityChartData();

	AJ.SetTopRulesChartData();

	AJ.SetEventsChartData();

	AJ.SetClosedEventsChartData();

	AJ.SetNotClosedEventsChartData();

	AJ.SetInErrorStateEventsChartData();

	AJ.SetQuantityVsVarietyChartData();

	dc.renderAll("MeterWidgets");

	AJ.Redraw();

};

AJ.CreateKwhVsChangeChart = function() {

	AJ.KwhVsChangeChart = dc.bubbleChart(AJ.Elements['widget-kwh-vs-change'], "MeterWidgets");

	AJ.KwhVsChangeChart
		.width(1000)
		.height(300)
		.margins({top: 10, left: 55, right: 50, bottom: 40})
		.transitionDuration(1500)
		.colors(["#68b8ff", "#ff0000", "#334479", "#ff7373", "#67e667", "#39e639", "#00cc00"])
		.colorDomain([0, 100])
		.colorAccessor(function (d) {
			return d.key % 100;
		})
		.r(d3.scale.linear().domain([10, 10]))
		.radiusValueAccessor(function (p) {
			return 10;
		})
		.yAxisLabel("% Change")
        .xAxisLabel("Consumption")
		.elasticY(true)
		.yAxisPadding(20)
		.elasticX(true)
		.xAxisPadding(500)
		.renderHorizontalGridLines(true)
		.renderLabel(false)
		.renderTitle(true);

	AJ.KwhVsChangeChart.on("filtered", function() {AJ.Redraw();});

	AJ.KwhVsChangeChart.yAxis()
		.tickFormat(function (v) {
            return v + "%";
        });

};

AJ.SetKwhVsChangeChartData = function() {

	var min_consumption = AJ.ConsumptionDimension.bottom(1)[0]["consumption"];
	var max_consumption = AJ.ConsumptionDimension.top(1)[0]["consumption"];

	AJ.KwhVsChangeChart
         .dimension(AJ.ConsumptionDimension)
         .group(AJ.ConsumptionDimensionSumByPercentChange)
         .x(d3.scale.linear().domain([min_consumption, max_consumption*1.01]))
         .y(d3.scale.linear().domain([0, 20]));

};

AJ.CreateTopTrendingOverBaseLineChart = function() {

	AJ.TopTrendingOverBaseLineRC = dc.rowChart(AJ.Elements['widget-top-trending-over-base-line-rc'], "MeterWidgets");

	AJ.TopTrendingOverBaseLineRC
		.width(500)
	    .height(17*10 + 25)
	    .margins({top: 20, left: 200, right: 10, bottom: 20})
		.elasticX(true)

		.colors(["#00abd6", "#00abd6", "#00abd6"])
		.colorDomain([1, 3])
		.colorAccessor(function() {return 2;})

		.labelOffsetX(-10)
		.labelOffsetY(8)
		.fixedBarHeight(10);

	AJ.TopTrendingOverBaseLineRC.xAxis()
		.ticks(4);

	AJ.TopTrendingOverBaseLineRC2 = dc.rowChart(AJ.Elements['widget-top-trending-over-base-line-bc'], "MeterWidgets");

	AJ.TopTrendingOverBaseLineRC2
		.width(500)
	    .height(17*10 + 25)
	    .margins({top: 20, left: 200, right: 10, bottom: 20})
		.elasticX(true)

		.colors(["#ed8600", "#ed8600", "#ed8600"])
		.colorDomain([1, 3])
		.colorAccessor(function() {return 2;})

		.labelOffsetX(-10)
		.labelOffsetY(8)
		.fixedBarHeight(10);

	AJ.TopTrendingOverBaseLineRC2.xAxis()
		.ticks(4);

};

AJ.SetTopTrendingOverBaseLineChartData = function() {

	AJ.TopTrendingOverBaseLineRC
		.dimension(AJ.FacilityNameDimension)
		.group(AJ.FacilityNameDimensionSum)
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value.ConsumptionSum; })
		.ordering(function(d){ return -d.value.ConsumptionSum; })
		.rowsCap(10)
		.othersGrouper(false);

	AJ.TopTrendingOverBaseLineRC2
		.dimension(AJ.FacilityNameDimension)
		.group(AJ.FacilityNameDimensionSum)
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value.ConsumptionSum; })
		.ordering(function(d){ return -d.value.ConsumptionSum; })
		.rowsCap(10)
		.othersGrouper(false);

};

AJ.CreateTopTrendingUnderBaseLineChart = function() {



};

AJ.SetTopTrendingUnderBaseLineChartData = function() {



};

AJ.CreateTopFacilityChart = function() {

	AJ.TopFacilityRC = dc.rowChart(AJ.Elements['widget-top-facilities-rc'], "MeterWidgets");

	AJ.TopFacilityRC
		.width(500)
	    .height(17*10 + 25)
	    .margins({top: 0, left: 200, right: 10, bottom: 20})
		.elasticX(true)
		.colors("#00abd6")
		.labelOffsetX(-10)
		.labelOffsetY(8)
		.fixedBarHeight(10);

	AJ.TopFacilityRC.xAxis()
		.ticks(4);

	AJ.TopFacilityRC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetTopFacilityChartData = function() {

	AJ.TopFacilityRC
		.dimension(AJ.FacilityNameDimension)
		.group(AJ.FacilityNameDimensionSum)
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value.EventsSum; })
		.ordering(function(d){ return -d.value.EventsSum; })
		.rowsCap(11)
		.othersGrouper(false);

};

AJ.CreateTopRulesChart = function() {

	AJ.TopRulesRC = dc.rowChart(AJ.Elements['widget-top-rules-rc'], "MeterWidgets");

	AJ.TopRulesRC
		.width(500)
	    .height(17*10 + 25)
	    .margins({top: 0, left: 200, right: 10, bottom: 20})
		.elasticX(true)
		.colors("#00abd6")
		.labelOffsetX(-10)
		.labelOffsetY(8)
		.fixedBarHeight(10);

	AJ.TopRulesRC.xAxis()
		.ticks(4);

	AJ.TopRulesRC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetTopRulesChartData = function() {

	AJ.TopRulesRC
		.dimension(AJ.RulesDimension)
		.group(AJ.RulesDimensionSum)
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value.EventsSum; })
		.ordering(function(d){ return -d.value.EventsSum; })
		.rowsCap(10)
		.othersGrouper(false);

};

AJ.CreateEventsChart = function() {

	AJ.EventsBC = dc.barChart(AJ.Elements['widget-events-bc'], "MeterWidgets");

	AJ.EventsBC
		.width(200)
	    .height(20)
	    .margins({top: 0, left: 0, right: 0, bottom: 0})
		.elasticX(true)
		.colors("#00abd6");

	AJ.EventsBC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetEventsChartData = function() {

	var min_date = AJ.DateDimension.bottom(1)[0]["Date"];
	var max_date = AJ.DateDimension.top(1)[0]["Date"];

	AJ.EventsBC
		.dimension(AJ.DateDimension)
		.group(AJ.DateDimensionSums)
		.x(d3.scale.linear().domain([min_date, max_date]))
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value['# Events'] > 0 ? p.value['# Events'] : 0; });

};

AJ.CreateClosedEventsChart = function() {

	AJ.ClosedEventsBC = dc.barChart(AJ.Elements['widget-closed-events-bc'], "MeterWidgets");

	AJ.ClosedEventsBC
		.width(200)
	    .height(20)
	    .margins({top: 0, left: 0, right: 0, bottom: 0})
		.elasticX(true)
		.colors("#00abd6");

	AJ.ClosedEventsBC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetClosedEventsChartData = function() {

	var min_date = AJ.DateDimension.bottom(1)[0]["Date"];
	var max_date = AJ.DateDimension.top(1)[0]["Date"];

	AJ.ClosedEventsBC
		.dimension(AJ.DateDimension)
		.group(AJ.DateDimensionSums)
		.x(d3.scale.linear().domain([min_date, max_date]))
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value['# Closed Events'] > 0 ? p.value['# Closed Events'] : 0; });

};

AJ.CreateNotClosedEventsChart = function() {

	AJ.NotClosedEventsBC = dc.barChart(AJ.Elements['widget-not-closed-events-bc'], "MeterWidgets");

	AJ.NotClosedEventsBC
		.width(200)
	    .height(20)
	    .margins({top: 0, left: 0, right: 0, bottom: 0})
		.elasticX(true)
		.colors("#00abd6");

	AJ.NotClosedEventsBC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetNotClosedEventsChartData = function() {

	var min_date = AJ.DateDimension.bottom(1)[0]["Date"];
	var max_date = AJ.DateDimension.top(1)[0]["Date"];

	AJ.NotClosedEventsBC
		.dimension(AJ.DateDimension)
		.group(AJ.DateDimensionSums)
		.x(d3.scale.linear().domain([min_date, max_date]))
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value['# Not Closed Events'] > 0 ? p.value['# Not Closed Events'] : 0; });

};

AJ.CreateInErrorStateEventsChart = function() {

	AJ.InErrorStateEventsBC = dc.barChart(AJ.Elements['widget-in-error-state-events-bc'], "MeterWidgets");

	AJ.InErrorStateEventsBC
		.width(200)
	    .height(20)
	    .margins({top: 0, left: 0, right: 0, bottom: 0})
		.elasticX(true)
		.colors("#00abd6");

	AJ.InErrorStateEventsBC.on("filtered", function() {AJ.Redraw();});

};

AJ.SetInErrorStateEventsChartData = function() {

	var min_date = AJ.DateDimension.bottom(1)[0]["Date"];
	var max_date = AJ.DateDimension.top(1)[0]["Date"];

	AJ.InErrorStateEventsBC
		.dimension(AJ.DateDimension)
		.group(AJ.DateDimensionSums)
		.x(d3.scale.linear().domain([min_date, max_date]))
		.keyAccessor(function(p) { return p.key; })
		.valueAccessor(function(p) { return p.value['# In Error State'] > 0 ? p.value['# In Error State'] : 0; });

};

AJ.CreateQuantityVsVarietyChart = function() {

	AJ.QuantityVsVarietyChart = dc.bubbleChart(AJ.Elements['widget-quantity-vs-variety-bc'], "MeterWidgets");

	AJ.QuantityVsVarietyChart
		.width(500)
		.height(250)
		.margins({top: 10, left: 55, right: 20, bottom: 30})
		.transitionDuration(1500)
		.colors(["#68b8ff", "#ff0000", "#334479", "#ff7373", "#67e667", "#39e639", "#00cc00"])
		.colorDomain([0, 100])
		.colorAccessor(function (d) {
			return d.key % 100;
		})
		.r(d3.scale.linear().domain([10, 10]))
		.radiusValueAccessor(function (p) {
			return 10;
		})
		.yAxisLabel("# of Events")
        .xAxisLabel("# of Rules")
		.elasticY(true)
		.yAxisPadding(0)
		.elasticX(true)
		.xAxisPadding(1)
		.renderVerticalGridLines(true)
		.renderLabel(false)
		.renderTitle(true);

	AJ.QuantityVsVarietyChart.on("filtered", function() {AJ.Redraw();});

	AJ.QuantityVsVarietyChart.yAxis();

	AJ.QuantityVsVarietyChart.xAxis();

};

AJ.SetQuantityVsVarietyChartData = function() {

	var min_rules = AJ.RulesDimension.bottom(1)[0]["# Rules"];
	var max_rules = AJ.RulesDimension.top(1)[0]["# Rules"];

	AJ.QuantityVsVarietyChart
    	.dimension(AJ.RulesDimension)
    	.group(AJ.RulesDimensionSum)
    	.x(d3.scale.linear().domain([min_rules, max_rules]))
    	.y(d3.scale.linear().domain([0, 20]))
		.valueAccessor(function(p) { return p.value['EventsSum'] > 0 ? p.value['EventsSum'] : 0; });

};


//Save links to DOM elements
AJ.ProcessLinks = function(el, holder) {

    var els = el.getElementsByTagName('*');

    for (var i=0; els[i]; i++) {

        var code = els[i].getAttribute("link");
        if (code) {

            holder[code] = els[i];

        }

    }

};




